class Record {
  constructor() {
    this.record = [];
  }

  store(data) {
    this.record.push(data);
  }
  get() {
    return this.record;
  }
}
module.exports = new Record();
