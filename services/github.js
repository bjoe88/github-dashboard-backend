const fetch = require('node-fetch');
const record = require('./record');
const config = require('../config/config')

class github {
  constructor() {
    this.itemPerPage = 10;
  }

  async search(q, language = null, topic = null, afterCursor = null, beforeCursor = null) {
    const s = this;
    let query = s.getQuery(q, language, topic, afterCursor, beforeCursor);
    let fetching = await fetch('https://api.github.com/graphql', {
      method: 'POST',
      body: JSON.stringify({ query }),
      headers: {
        'Authorization': `Bearer ${config.githubApiKey}`,
      },
    });

    var data = await fetching.text();
    //Temporary storage.
    record.store({
      query: q,
      language: language,
      topic: topic,
      afterCursor: afterCursor,
      beforeCursor: beforeCursor,
      date: new Date(),
      data: data
    });
    return data;
  }

  getQuery(q, language = null, topic = null, afterCursor = null, beforeCursor = null) {
    const s = this;
    let searchTerm = q;
    let after = '';
    let numberOfItem = '';
    let before = '';
    if (!!language)
      searchTerm += ' language:' + language;
    if (!!topic)
      searchTerm += ' topic:' + topic;
    if (!!afterCursor) {
      numberOfItem = 'first: ' + s.itemPerPage + ','
      after = ' after:"' + afterCursor + '", ';
    }
    else if (!!beforeCursor) {
      numberOfItem = 'last: ' + s.itemPerPage + ','
      before = ' before:"' + beforeCursor + '", ';
    }
    return `
    query {
      search(query: "${searchTerm}", 
             ${numberOfItem}
             ${after}
             ${before}
             type: REPOSITORY
             ) {
          repositoryCount
          pageInfo {
            endCursor
            hasNextPage
            hasPreviousPage
            startCursor
          }
          edges {
            cursor
            node {
              ... on Repository {
                id
                name
                descriptionHTML
                primaryLanguage {
                  name
                  color
                }
                repositoryTopics (first:10) {
                  edges {
                    node {
                      id
                      topic  {
                        id
                        name
                      }
                    }
                  }
                }
              }
            }
          }
        }
    }`;
  }
}
module.exports = new github();
