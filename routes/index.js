var express = require('express');
var router = express.Router();
var github = require('../services/github')
var record = require('../services/record')


/* GET home page. */
router.get('/', async function (req, res, next) {
  let q = req.query.q;
  let language = req.query.language;
  let topic = req.query.topic;
  let after = req.query.after;
  let before = req.query.before;
  var text = await github.search(q, language, topic, after, before);
  res.json(JSON.parse(text));
});

router.get('/get-search-record', async function (req, res, next) {
  var data = await record.get()
  data = data.reverse();
  data = data.map((ele) => {
    return {
      query: ele.query,
      language: ele.language,
      topic: ele.topic,
      date: ele.date,
      data: ele.data
    }
  })
  res.json(data)
});

module.exports = router;
