# github-dashboard-backend

This will have the backend service for the fetch the data from the github api v4 and store the result for the admin part.


# Configuration

The configuration file located at `src/environments/environment.ts` for development config and `src/environments/environment.prod.ts` for production config. Since this is frontend component. It should only store config that is not sensitive.


# Installation

This service should be run in using container. There is a file docker-compose.yml at the Main repo `https://gitlab.com/bjoe88/github-dashboard.git`.